using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject character;
    public GameObject InvisibleUp;
    private Vector3 offSet;
    private Vector3 maxPos;
    private Camera myCamera;
    private float cameraMaxHeight;
    // Start is called before the first frame update
    void Start()
    {
        myCamera = GetComponent<Camera>();
        cameraMaxHeight = InvisibleUp.transform.position.y - myCamera.orthographicSize;
        offSet = new Vector3(-transform.position.x, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(character.transform.position.y + offSet.y > cameraMaxHeight)
        {
            maxPos = new Vector3(
                (float)character.transform.position.x + offSet.x,
                cameraMaxHeight,
                transform.position.z);
            transform.position = maxPos;
        }
        else
        {
            transform.position = character.transform.position + offSet;
        }
        
    }
}
