using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] toPhysicObj;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        for(int i = 0; i < toPhysicObj.Length; i++)
        {
            toPhysicObj[i].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }   
        Destroy(gameObject);
    }
}
