using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{ 
    public AudioSource JumpingSound;
    public AudioSource WalkingSound;
    public AudioSource backgroundMusic;
    

    // Start is called before the first frame update
    void Start()
    {

    }
    
    // Update is called once per frame
    void Update()
    {

    }
    public void WalkingSoundUnpause()
    {
        if (WalkingSound.isPlaying == false)
        {
            WalkingSound.UnPause();
        }
    }
    public void BackgroundSoundVolume(Slider slider)
    {
        backgroundMusic.volume = slider.value;
    }
    public void SoundEffectVolume(Slider slider)
    {
        JumpingSound.volume = slider.value;
        WalkingSound.volume = slider.value;
    }
}
