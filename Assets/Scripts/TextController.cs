using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextController : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timePassedText;
    int score;
    float timePassed;
    void Start()
    {
        score = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        timePassed = Time.realtimeSinceStartup;
        scoreText.text = "Score: " + score;
        timePassedText.text = "Time: " + (int)timePassed;
    }
}
