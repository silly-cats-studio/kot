using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlatformMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public int movementLength;
    public int speed;
    public int orientation; // -1 - Left; 1 - Right

    private Vector2 movementForward;
    private float platformZeroPos;
    private float platformCurrentPos;
    private float translation;

    void Start()
    {
        platformZeroPos = transform.position.x;
        platformCurrentPos = transform.position.x;
        movementForward = new Vector2(orientation, 0 );
    }

    // Update is called once per frame
    void Update()
    {
        if(platformCurrentPos < platformZeroPos+movementLength)
        {
            translation = Time.deltaTime * speed;
            transform.Translate(movementForward * translation);
            platformCurrentPos += translation;
        }
        else if(platformCurrentPos >= platformZeroPos + movementLength)
        {
            platformCurrentPos = platformZeroPos;
            orientation = -(orientation);
            movementForward = new Vector2(orientation, 0);
        }

    }
}
