using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    //private Collider2D pointObject;
    public GameOver pointCheck;
    void Start()
    {
        //pointObject = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        pointCheck.score++;
        Destroy(gameObject);
    }
}
