using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update

    public SoundManager soundManager;
    public int jumpHeight = 10;
    public float movementSpeed = 5;
    private bool canJump = false;
    public bool gameOver;
    SpriteRenderer spriteRenderer;
    Rigidbody2D playerRigidBody;
    public Collider2D DangerousZoneCollider;

    public Vector2 startPoint;
    
    void Start()
    {
        gameOver = false;
        startPoint = transform.position;
        spriteRenderer = GetComponent<SpriteRenderer>();
        playerRigidBody = GetComponent<Rigidbody2D>();

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Platforms")
        {
            canJump = true;
        }
        else if (collision.gameObject.tag == "DangerousZone")
        {
            transform.position = startPoint;
            gameOver = true;
        }
    }
    // Update is called once per frame
    private void Walking()
    {
        float xPos = Input.GetAxis("Horizontal");
        if (xPos < 0)
        {
            spriteRenderer.flipX = true;
            soundManager.WalkingSoundUnpause();
        }
        else if (xPos == 0)
        {
            soundManager.WalkingSound.Pause();
        }
        else
        {
            spriteRenderer.flipX = false;
            soundManager.WalkingSoundUnpause();
        }
        Vector2 movement = new Vector2(xPos, 0);
        transform.Translate(movement * movementSpeed * Time.deltaTime);
    }
    private void Jumping()
    {
        if (Input.GetKeyDown(KeyCode.Space) && canJump)
        {
            playerRigidBody.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);
            soundManager.JumpingSound.Play();
            canJump = false;
        }
    }
    void Update()
    {
        if(gameOver == false)
        {
            Walking();
            Jumping();
        }
    }
        
}
