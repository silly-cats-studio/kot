using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    public Movement player;
    public SoundManager soundManager;
    public Button startAgainButton;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timePassedText;
    public int score;
    float timePassed;
    void Start()
    {
        soundManager.backgroundMusic.Play();
        soundManager.WalkingSound.Play();
        score = 0;
        timePassed = Time.timeSinceLevelLoad;
        
    }
    void IsGameOver()
    {
        if (player.gameOver == true)
        {
            soundManager.backgroundMusic.Stop();
            soundManager.WalkingSound.Stop();
            startAgainButton.gameObject.SetActive(true);
        }
        else
        {
            UITextController();
        }
    }
    /*
    void ShiftKeyDown()
    {
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            player.gameOver = false;
            soundManager.backgroundMusic.Play();
            soundManager.WalkingSound.Play();
            timePassed = Time.time;
        }
    }*/
    void UITextController()
    {
        timePassed = Time.timeSinceLevelLoad;
        scoreText.text = "Score: " + score;
        timePassedText.text = "Time: " + (int)timePassed;
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    // Update is called once per frame
    void Update()
    {
        IsGameOver();
        //ShiftKeyDown();
        
    }
}
